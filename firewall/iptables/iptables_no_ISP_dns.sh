# Bloc connection to ISP's DNS
iptables -I INPUT --src 103.86.99.10,60.109.121.33,ns-c-brln-04.net.telefonica.de -j DROP  # set here your ISP's DNS addresses
iptables -I OUTPUT --dst 103.86.99.10,62.109.121.33,ns-c-brln-04.net.telefonica.de -j DROP  # set here your ISP's DNS addresses 
